# Information Retrieval 2016 - Group 23

## How To Run

1. Download non `sbt` libraries from `https://polybox.ethz.ch/index.php/s/9Qqfn2PD3zWOUGd` and extract into `lib` directory.
2. Change `model` (src/main/scala/Main.scala:14) to `'t'` or `'l'` depending on which model shoould be used.
3. (If needed) adjust other parameters
4. Start execution with `sbt run`.
5. Results should appear in the console.
