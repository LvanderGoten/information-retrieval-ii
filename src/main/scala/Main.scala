import java.io._

import breeze.numerics._
import ch.ethz.dal.tinyir.io.TipsterStream
import ch.ethz.dal.tinyir.processing.{StopWords, XMLDocument}
import com.github.aztek.porterstemmer._
import org.apache.commons.io.FileUtils
import org.iq80.leveldb._
import org.iq80.leveldb.impl.Iq80DBFactory._

object Main extends App {
  println("ETHZ Information Retrieval 2016 - Project 2")

  val model = 'l' // 't' for  term-based model, 'l' for language model.
  val numDocuments = 100000
  val dataDir = "data"
  val trainingQueriesFile = "questions-descriptions.txt"
  val testQueriesFile = "test-questions.txt"
  val judgementFile = "relevance-judgements.csv"
  val dbFile = "db"
  val dbClean = false
  val training = false

  // Best parameters
  val alpha_dash = 0.5
  val beta_dash = 0.65
  val lambda_dash = 0.05

  val rankingCutoff = 100
  val rankingThreshold = 5.0

  val dbFileWithDir = dataDir + "/" + dbFile
  val dbFileObj = new File(dbFileWithDir)
  // Remove database if configured to
  if (dbClean)
    if (dbFileObj.exists())
      FileUtils.deleteDirectory(dbFileObj)
  // Create database
  val dbOptions = new Options
  dbOptions.createIfMissing(true)
  val db = factory.open(new File(dbFileWithDir), dbOptions)

  case class TfTuple(term: String, docId: String, count: Int)

  try {
    val trainQueries: Stream[Query] = QueryReader.readFile(dataDir + "/" + trainingQueriesFile)
    val testQueries: Stream[Query] = QueryReader.readFile(dataDir + "/" + testQueriesFile)
    println("Imported queries")

    val judgements = io.Source.fromFile(dataDir + "/" + judgementFile).getLines().filter(l => !l.endsWith("0"))
      .map(l => l.split(" ")).map(lp => (lp(0).toInt, lp(2).replaceAll("-", ""))).toList.groupBy(_._1)
      .mapValues(_.map(_._2))
    println("Imported judgements")

    def get_tf_tuples(docs: Stream[RichDocument]): Stream[TfTuple] = {
      docs.flatMap(d => d.tokens.groupBy(identity).map({
        case (tk, lst) => TfTuple(tk, d.id, lst.length)
      }))
    }

    val doc_st: Stream[XMLDocument] = new TipsterStream(dataDir).stream
    val rich_docs = Preprocessor.unfiltered(doc_st.take(numDocuments))

    // Frequency index
    if (dbClean) {
      println()
      println("Building frequency index")
      val fq_index: Map[String, Stream[(String, Int)]] = get_tf_tuples(rich_docs)
        .groupBy(_.term)
        .mapValues(_.map((tf_tuple: TfTuple) => (tf_tuple.docId, tf_tuple.count)))

      for (fq_key <- fq_index.keys) {
        // Information about token
        val strinfo = fq_index(fq_key).map(x => "%s:%d".format(x._1, x._2)).mkString(" ")
        // Put information into database
        db.put(bytes("t:" + fq_key), bytes(strinfo))
      }

      println()
      println("Doc Lens.")
      for (rich_doc <- rich_docs) {
        db.put(bytes("d:" + rich_doc.id), bytes(rich_doc.tokens.length.toString))
      }
      db.put(bytes("d:avg"), bytes((rich_docs.map(d => d.tokens.length).sum / rich_docs.size.toDouble).toString))
    }

    if (model == 'l') {
      if (training) {
        println("Training language-based model")

        // Grid search
        //val lambdas = scala.collection.immutable.Range.Double.apply(0.0, 0.101, 0.02) ++ scala.collection.immutable.Range.Double.apply(0.27, 0.331, 0.02) ++ scala.collection.immutable.Range.Double.apply(0.77, 0.831, 0.02)
        val lambdas = scala.collection.immutable.Range.Double.apply(0.67, 0.831, 0.02)

        var lambda_hat = lambdas(0)
        var best_map = 0.0
        var best_precision = 0.0
        var best_recall = 0.0
        var best_f_score = 0.0

        for (lambda <- lambdas) {
          println("Lambda: " + lambda)

          val predictions: Stream[Map[String, Double]] = new Predictor('l', _lambda = lambda).predict(db, trainQueries, numDocuments)

          val ranking: Stream[Seq[(String, Double)]] = predictions
            .map((m: Map[String, Double]) => m.toVector.sortBy(_._2) ++ rich_docs.filter((d: RichDocument) => !m.contains(d.id)).map((d: RichDocument) => (d.id, 0.0)))
          val results: Stream[Seq[String]] = ranking.map(_.map(_._1).take(rankingCutoff))
          //val results:Stream[Seq[String]] = ranking.map(_.filter(_._2 > rankingThreshold).map(_._1))
          val ground_truth: Stream[Seq[String]] = trainQueries.map((q: Query) => judgements(q.id))

          //val best_thresholds:Stream[Double] = ranking.zip(ground_truth)
          //  .map((f: (Seq[(String, Double)], Seq[String])) => f._1(f._2.length - 1)._2)
          //val best_threshold:Double = best_thresholds.sum / best_thresholds.length.toDouble

          // Determine precision, recall, F-score
          val avg_precision = Postprocessor.avg_precision(ground_truth.map(_.toSet), results.map(_.toSet))
          val avg_recall = Postprocessor.avg_recall(ground_truth.map(_.toSet), results.map(_.toSet))
          val avg_f_score = Postprocessor.avg_f_score(ground_truth.map(_.toSet), results.map(_.toSet))
          //val ap_per_query = Postprocessor.ap_per_query(ground_truth, ranking.map(_.map(_._1)))
          val map_score = Postprocessor.map_score(ground_truth, results)

          //println("Best threshold %f" format(best_threshold))
          //println("Average precision %f" format(avg_precision))
          //println("Average recall %f" format(avg_recall))
          //println("Average F-Score %f" format(avg_f_score))
          //println("MAP %f" format(map_score))

          // Update
          if (best_map < map_score) {
            best_map = map_score
            best_precision = avg_precision
            best_recall = avg_recall
            best_f_score = avg_f_score
            lambda_hat = lambda

            println("[BEST] lambda = %f, Map = %f, Precision = %f, Recall = %f, F-Score = %f" format(lambda_hat, best_map, best_precision, best_recall, best_f_score))
          } else {
            println("[...] lambda = %f, Map = %f, Precision = %f, Recall = %f, F-Score = %f" format(lambda, map_score, avg_precision, avg_recall, avg_f_score))
          }
        }

        // Print best combination
        println("[FINAL] Best lambda = %f, Best Map score = %f" format(lambda_hat, best_map))
      } else {
        println("Testing language-based model")
        val predictions: Stream[Map[String, Double]] = new Predictor('t', alpha_dash, beta_dash).predict(db, testQueries, numDocuments)
        val ranking: Stream[Seq[(String, Double)]] = predictions
          .map((m: Map[String, Double]) => m.toVector.sortBy(_._2) ++ rich_docs.filter((d: RichDocument) => !m.contains(d.id)).map((d: RichDocument) => (d.id, 0.0)))
        val results: Stream[Seq[String]] = ranking.map(_.map(_._1).take(rankingCutoff))

        for (c <- testQueries zip results) {
          val query_id = c._1.id

          for (st <- c._2.zipWithIndex) {
            println("%d %d %s" format(query_id, st._2 + 1, st._1))
          }
        }
      }
    } else {
      if (training) {
        println("Using term-based model")
        // Grid search
        val alphas = scala.collection.immutable.Range.Double.apply(0.0, 1.5, 0.1)
        val betas = scala.collection.immutable.Range.Double.apply(0.5, 1.5, 0.05)

        var alpha_hat = alphas(0)
        var beta_hat = betas(0)
        var best_map = 0.0
        var best_precision = 0.0
        var best_recall = 0.0
        var best_f_score = 0.0

        for (alpha <- alphas) {
          for (beta <- betas) {
            val predictions: Stream[Map[String, Double]] = new Predictor('t', alpha, beta, 0.0).predict(db, trainQueries, numDocuments)

            val ranking: Stream[Seq[(String, Double)]] = predictions
              .map((m: Map[String, Double]) => m.toVector.sortBy(-_._2) ++ rich_docs.filter((d: RichDocument) => !m.contains(d.id)).map((d: RichDocument) => (d.id, 0.0)))
            val results: Stream[Seq[String]] = ranking.map(_.map(_._1).take(rankingCutoff))
            //val results:Stream[Seq[String]] = ranking.map(_.filter(_._2 > rankingThreshold).map(_._1))
            val ground_truth: Stream[Seq[String]] = trainQueries.map((q: Query) => judgements(q.id))

            //val best_thresholds:Stream[Double] = ranking.zip(ground_truth)
            //  .map((f: (Seq[(String, Double)], Seq[String])) => f._1(f._2.length - 1)._2)
            //val best_threshold:Double = best_thresholds.sum / best_thresholds.length.toDouble

            // Determine precision, recall, F-score
            val avg_precision = Postprocessor.avg_precision(ground_truth.map(_.toSet), results.map(_.toSet))
            val avg_recall = Postprocessor.avg_recall(ground_truth.map(_.toSet), results.map(_.toSet))
            val avg_f_score = Postprocessor.avg_f_score(ground_truth.map(_.toSet), results.map(_.toSet))
            //val ap_per_query = Postprocessor.ap_per_query(ground_truth, ranking.map(_.map(_._1)))
            val map_score = Postprocessor.map_score(ground_truth, results)

            //println("Best threshold %f" format(best_threshold))
            //println("Average precision %f" format(avg_precision))
            //println("Average recall %f" format(avg_recall))
            //println("Average F-Score %f" format(avg_f_score))
            //println("MAP %f" format(map_score))

            // Update
            if (best_map < map_score) {
              best_map = map_score
              best_precision = avg_precision
              best_recall = avg_recall
              best_f_score = avg_f_score
              alpha_hat = alpha
              beta_hat = beta

              println("[BEST] alpha = %f, beta = %f, Map = %f, Precision = %f, Recall = %f, F-Score = %f" format(alpha_hat, beta_hat, best_map, best_precision, best_recall, best_f_score))
            }
          }
        }

        // Print best combination
        println("[FINAL] Best alpha = %f, Best beta = %f, Best Map score = %f" format(alpha_hat, beta_hat, best_map))
      } else {
        val predictions: Stream[Map[String, Double]] = new Predictor('t', alpha_dash, beta_dash).predict(db, testQueries, numDocuments)
        val ranking: Stream[Seq[(String, Double)]] = predictions
          .map((m: Map[String, Double]) => m.toVector.sortBy(-_._2) ++ rich_docs.filter((d: RichDocument) => !m.contains(d.id)).map((d: RichDocument) => (d.id, 0.0)))
        val results: Stream[Seq[String]] = ranking.map(_.map(_._1).take(rankingCutoff))

        for (c <- testQueries zip results) {
          val query_id = c._1.id

          for (st <- c._2.zipWithIndex) {
            println("%d %d %s" format(query_id, st._2 + 1, st._1))
          }
        }
      }
    }

    println("Used documents: " + numDocuments)
  } finally {
    db.close()
  }

  def docLens(docId: String): Int = {
    val response = db.get(bytes("d:" + docId))

    if (response != null) {
      return asString(response).toInt
    }

    0
  }
}

case class RichDocument(tokens: Seq[String], id: String)

object Preprocessor {
  val TfIdfPercentile: Int = 25

  def unfiltered(doc_st: Stream[XMLDocument]): Stream[RichDocument] = {
    doc_st.map((x: XMLDocument) => RichDocument(get_tokens(x.tokens), x.name))
  }

  def prune_tf_idf(doc_st: Stream[XMLDocument]): Stream[RichDocument] = {

    val n: Int = doc_st.length
    val token_st: Stream[Seq[String]] = doc_st.map((doc: XMLDocument) => get_tokens(doc.tokens))

    // Create TF maps
    println("Create TF maps")
    val tf_st: Stream[Map[String, Double]] = token_st.map((f: Seq[String]) => f.groupBy(identity).mapValues(l => log2(1.0 + l.length.toDouble)))

    // Determine vocabulary
    println("Determine vocabulary")
    val vocab: Stream[String] = token_st.flatten.distinct

    val token_unique_st: Stream[Set[String]] = token_st.map(_.toSet)

    // Count in how many documents a word is (IDF)
    println("Determine IDF")
    //val df_seq: Stream[Double] = vocab
    //  .map((word: String) => log2(n.toDouble) - log2(token_unique_st.count((tokens: Set[String]) => tokens contains word).toDouble))
    //val df_map: Map[String, Double] = vocab.zip(df_seq).toMap
    val df_map: Map[String, Double] = vocab.map((word: String) => (word, log2(n.toDouble) - log2(token_unique_st.count((tokens: Set[String]) => tokens contains word).toDouble))).toMap

    // Construct TF-IDF map
    println("Construct TF-IDF map")
    val tf_idf_st: Stream[Map[String, Double]] = tf_st.map((m: Map[String, Double]) => m.toSeq.map((t: (String, Double)) => (t._1, df_map(t._1) * t._2)).toMap)

    // Construct ranking
    println("Construct ranking")
    val word_ranking_seq: Stream[Double] = vocab.map((word: String) => tf_idf_st.map((m: Map[String, Double]) => m.getOrElse(word, 0.0)).sum)

    // Sorted ranking
    val word_ranking = vocab.zip(word_ranking_seq).sortBy(_._2).map(_._2).toIndexedSeq
    val _min = word_ranking(0)
    val _max = word_ranking.last
    val _median = word_ranking(word_ranking.length / 2)
    val _3Q = word_ranking((3 * word_ranking.length) / 4)
    println("[Min = %f, Max = %f, Median = %f, 3Q = %f]" format(_min, _max, _median, _3Q))

    // Words to be removed
    println("Prune words")
    val _threshold = word_ranking((TfIdfPercentile * word_ranking.length) / 100)
    val bad_words: Stream[String] = vocab.zip(word_ranking_seq).filter(_._2 <= _threshold).sortBy(_._2).map(_._1)
    println("Pruned " + bad_words.length + " words")

    //return token_st.zipWithIndex.map(f => RichDocument(f._1.filter(!bad_words.contains(_)), f._2))

    // TODO: Change when ID is correct!!!
    token_st.zip(doc_st).map(f => RichDocument(f._1.filter(!bad_words.contains(_)), f._2.name))
  }

  def get_tokens(tokens: Seq[String]): Seq[String] = {
    StopWords.filterOutSW(tokens)
      .filter((token: String) => !token.exists(_.isDigit))
      .map(_.toLowerCase)
      .map(PorterStemmer.stem)
  }

  def docLens(db: DB, docId: String): Int = {
    val response = db.get(bytes("d:" + docId))

    if (response != null) {
      return asString(response).toInt
    }

    0
  }
}

object Postprocessor {
  def avg_precision(real_documents: Seq[Set[String]], predicted_documents: Seq[Set[String]]): Double = {
    precision_per_query(real_documents, predicted_documents).sum / real_documents.length.toDouble
  }

  def avg_recall(real_documents: Seq[Set[String]], predicted_documents: Seq[Set[String]]): Double = {
    recall_per_query(real_documents, predicted_documents).sum / real_documents.length.toDouble
  }

  def avg_f_score(real_documents: Seq[Set[String]], predicted_documents: Seq[Set[String]]): Double = {
    f_score_per_query(real_documents, predicted_documents).sum / real_documents.length.toDouble
  }

  def precision_per_query(real_documents: Seq[Set[String]],
                          predicted_documents: Seq[Set[String]]): Seq[Double] = {

    real_documents.zip(predicted_documents)
      .map((f: (Set[String], Set[String])) => if (f._2.isEmpty) 0.0 else (f._1 & f._2).size.toDouble / f._2.size.toDouble)
  }

  def recall_per_query(real_documents: Seq[Set[String]],
                       predicted_documents: Seq[Set[String]]): Seq[Double] = {

    real_documents.zip(predicted_documents)
      .map((f: (Set[String], Set[String])) => (f._1 & f._2).size.toDouble / f._1.size.toDouble)
  }

  def map_score(real_documents: Seq[Seq[String]], predicted_documents: Seq[Seq[String]]): Double = {
    val n_queries: Int = real_documents.length

    ap_per_query(real_documents, predicted_documents).sum / n_queries.toDouble
  }

  def ap_per_query(real_documents: Seq[Seq[String]],
                   predicted_documents: Seq[Seq[String]]): Seq[Double] = {
    val n: Int = predicted_documents.head.length

    real_documents.zip(predicted_documents)
      .map({
        case (real_document, predicted_document) => List.range(0, n)
          .map((k: Int) => precision_at_k(real_document, predicted_document, k) * (if (real_document contains predicted_document(k)) 1.0 else 0.0)).sum / real_document.length.toDouble
      })
  }

  def precision_at_k(real_document: Seq[String],
                     predicted_document: Seq[String], k: Int): Double = {
    precision_per_query(Seq(real_document.take(k).toSet), Seq(predicted_document.toSet)).head
  }

  def f_score_per_query(real_documents: Seq[Set[String]],
                        predicted_documents: Seq[Set[String]],
                        beta: Double = 1.0): Seq[Double] = {

    precision_per_query(real_documents, predicted_documents)
      .zip(recall_per_query(real_documents, predicted_documents))
      .map((f: (Double, Double)) => if (f._1 == 0.0 && f._2 == 0.0) 0.0 else (beta * beta + 1) * f._1 * f._2 / (beta * beta * f._1 + f._2))
  }
}
