import ch.ethz.dal.tinyir.processing.Tokenizer

class Query(_id: Int, _query: String) {
  def id = _id

  def query = _query

  val tokens = Tokenizer.tokenize(_query) // TODO: Maybe filter out stop words

  override def toString = "[" + id + "] " + tokens + " ('" + _query + "')"
}

object QueryReader {
  def readFile(path: String): Stream[Query] = {
    io.Source.fromFile(path).getLines().map(l => l.split(":", 2)).map(lp => new Query(lp(0).toInt, lp(1))).toStream
  }
}
