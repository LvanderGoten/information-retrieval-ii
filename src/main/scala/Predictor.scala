import Main.TfTuple
import breeze.numerics._
import org.iq80.leveldb.DB
import org.iq80.leveldb.impl.Iq80DBFactory._

class Predictor(_mode: Char = 't', _alpha: Double = 1.5, _beta: Double = 0.75, _lambda: Double = 0.0) {
  // 't' for  term-based model, 'l' for language model.
  val mode:Char = _mode

  // BM25 Parameters
  val alpha:Double = _alpha
  val beta:Double = _beta

  // Language model
  val lambda: Double = _lambda

  def predict(db: DB, queries: Stream[Query], numDocuments: Int): Stream[Map[String, Double]] = {
    // Construct frequency map for each query
    val queries_tf_tuples:Stream[Seq[TfTuple]] = queries.map((q: Query) => Preprocessor.get_tokens(q.tokens).flatMap((token: String) => get_db_tf_tuple(db, token)))

    // TODO: Update doc length assignment

    var query_scores:Stream[Map[String, Double]] = Stream.empty
    if (mode == 'l') {
      query_scores = queries_tf_tuples.map((query: Seq[TfTuple]) => language_model_score(query))
    } else {
      val avgDocLen = asString(db.get(bytes("d:avg"))).toDouble
      val queries_df_terms:Stream[Map[String, Double]] = queries_tf_tuples.map((query_tuples: Seq[TfTuple]) =>
        query_tuples
          .groupBy(_.term)
          .map((term_occ: (String, Seq[TfTuple])) => (term_occ._1, getIDF(term_occ._2.length, numDocuments))))
      query_scores = queries_tf_tuples.zip(queries_df_terms).map((f: (Seq[TfTuple], Map[String, Double])) =>
        f._1.groupBy(_.docId).mapValues((tf_tuples: Seq[TfTuple]) => tf_tuples.map((x: TfTuple) => f._2(x.term) * getFraction(x.count, docLens(db, x.docId), avgDocLen)).sum))
    }

    query_scores
  }
  // Using Jelinek-Mercer smoothing
  def language_model_score(query_tf_tuples: Seq[TfTuple]) = {
    // collection frequencies is calculated by grouping the tuples by tokens and taking the sum of the counts
    val cf: Iterable[Int] = query_tf_tuples.groupBy(_.term).mapValues(_.map(_.count).sum).values

    // term frequencies maps from doc ID to a 'local cf' for a single document
    val tf: Map[String, Iterable[Int]] = query_tf_tuples.groupBy(_.docId).mapValues(_.groupBy(_.term).mapValues(_.map(_.count).sum).values)
    val collection_size: Int = cf.sum

    // Smoothing the term frequency with the collection frequency according to the Jelinek-Mercer smoothing algorithm to get our prediction for P(w|d)
    // lambda_d was chosen as constant.
    tf.mapValues((doc_tf: (Iterable[Int])) => doc_tf.zip(cf).map((f: (Int, Int)) => log((1-lambda)*f._1/doc_tf.sum + lambda*f._2/collection_size)).sum)
  }

  def get_db_tf_tuple(db: DB, term:String): Seq[TfTuple] = {
    val response = db.get(bytes("t:" + term))

    if (response != null) {
      return asString(response)
        .split(' ')
        .map(s => s.split(":"))
        .map(e => TfTuple(term, e(0), e(1).toInt))
    }

    Seq.empty[TfTuple]
  }

  def docLens(db: DB, docId: String): Int = {
    val response = db.get(bytes("d:" + docId))

    if (response != null) {
      return asString(response).toInt
    }

    0
  }

  def getIDF(token_df: Int, numDocuments: Int):Double = log((numDocuments - token_df + 0.5)/(token_df + 0.5))

  def getFraction(tf: Int, docLen: Int, avgDocLen: Double):Double = {
    tf * (alpha + 1.0) / (tf + alpha * (1 - beta + beta * docLen.toDouble/avgDocLen))
  }
}
